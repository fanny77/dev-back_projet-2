<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>My Scoreboard</title>

	<!-- STYLES -->
	<link rel="stylesheet" href="/css/style.css">

	<!-- FONTS -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

	<!-- ASSETS -->
	<link href="https://fonts.googleapis.com/css2?family=Assistant:wght@300;400;500&family=Lora&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
</head>
<body>
<header>
	<nav>
		<ul>
			<li class="title"><a href="/">My Scoreboard</a></li>
			<li><a href="/ajouterJeux">Jeux<i class="fas fa-sort-down"></i></a></li>
			<li><a href="/ajouterJoueurs">Joueurs<i class="fas fa-sort-down"></i></a></li>
			<li><a href="/ajouterMatch">Matchs<i class="fas fa-sort-down"></i></a></li>
		</ul>
	</nav>
</header>

