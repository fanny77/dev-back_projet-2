<?php include '../Vues/header.php'; ?>

<section class="ajout-matchs">
	<h1>Ajout d'un match</h1>
	<form method="post" action="">

		<label for="jeux">Choix du jeux</label>
		<select id="jeux" name="jeux">
			<?php foreach($games as $game){	?>
				<option value="<?php echo $game->id ?>"><?php echo $game->title ?></option>
			<?php } ?>
		</select>

		<label for="date">Date de démarrage</label>
		<input type="date" name="date" id="date" required>

		<input type="submit" value="Ajouter un match">
	</form>

</section>
</body>

</html>
