<?php include '../Vues/header.php'; ?>
<section class="ajout-jeux">
	<h1>Ajout d'un jeux</h1>

	<form method="post" action="">
		<label for="name">Nom du jeu</label>
		<input type="text" id="name" name="name" required>

		<label for="min_players">Nombre de joueurs minimum</label>
		<input type="number" name="min_players" id="min_players" required>

		<label for="max_players">Nombre de joueurs maximum</label>
		<input type="number" name="max_players" id="max_players" required>

		<input type="submit" value="Ajouter un jeu">
	</form>

</section>
</body>

</html>
