<?php include '../Vues/header.php'; ?>
<section class="hero">
	<h1 class="title">My Scoreboard</h1>
	<p>Tenez à jour vos résultats entre amis</p>
</section>

<hr>

<section class="content">
	<article class="joueurs_section">
		<h2>Joueurs</h2>
		<table class="joueurs">
			<thead>
				<tr>
					<th>#</th>
					<th>Email</th>
					<th>Pseudo</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($players as $player) {
			?>
				<tr>
					<td><?php echo $player->id; ?></td>
					<td><?php echo $player->email; ?></td>
					<td><?php echo $player->nickname; ?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</article>
	<article class="jeux_section">
		<h2>Jeux</h2>
		<table class="jeux">
			<thead>
				<tr>
					<th>#</th>
					<th>Jeux</th>
					<th>Joueurs</th>
				</tr>
			</thead>
			<tbody>
		<?php foreach ($games as $game) {
		?>
			<tr>
				<td><?php echo $game->id; ?></td>
				<td><?php echo $game->title; ?></td>
				<td>De <?php echo $game->min_players; ?> à <?php echo $game->max_players; ?></td>
			</tr>
		<?php
		}
		?>
			</tbody>
		</table>
	</article>
	<article class="matchs_section">
		<h2>Matchs</h2>
		<table class="matchs">
			<thead>
				<tr>
					<th>#</th>
					<th>Jeux</th>
					<th>Infos</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($matchs as $match) {
				?>
					<tr class="<?php echo ($match->getStart_date() > new \DateTime()) ? 'shaded' :  '' ?>">
						<td> <?php echo $match->id ?> <a href="/updateMatch/<?php echo $match->id ?>" class="edit">éditer</a>
						</td>
						<td><?php echo $match->getGame()->title ?></td>
						<td>
							<p class="registeredPlayers">
							<?php
								echo $match->getNbPlayers();
							?> joueurs inscrits</p>
							<p class="stratedAt">A commencé le <?php echo $match->getStart_date()->format("d/m/Y") ?></p>
							<?php
							if ($match->winner_id) {
								?><p class="Winner"><?php echo "Gagné par ".$match->getWinner()->nickname ?></p>
							<?php
							}
							?>
						</td>
					</tr>
				<?php
				} ?>
			</tbody>
		</table>
	</article>
</section>
</body>

</html>
