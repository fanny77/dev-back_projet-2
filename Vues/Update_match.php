<?php include '../Vues/header.php'; ?>
<section class="single_match">

	<section class="game_section">
		<h2>Nom du jeu : <?php echo $game->title ?></h2>
		<p>Entre <?php echo $game->min_players ?> et <?php echo $game->max_players ?> joueurs</p>
		<p>Date de démarrage : <?php echo $match->getStart_date()->format("d/m/Y") ?> </p>
		<p>Gagnant : <?php echo ($match->getWinner() != null) ? $match->getWinner()->nickname : "--"; ?></p>

		
	</section>

	<article class="joueurs_section">
		<h2>Joueurs</h2>
		<table class="joueurs">
			<thead>
				<tr>
					<th>#</th>
					<th>Email</th>
					<th>Pseudo</th>
					<th>Supprimer le joueur</th>
				</tr>
			</thead>
			<tbody>

			<?php foreach ($players as $player) {
			?>
				<tr>
					<td><?php echo $player->id; ?></td>
					<td><?php echo $player->email; ?></td>
					<td><?php echo $player->nickname; ?></td>
					<td> <a class="trash" href="/updateMatch/<?php echo $match->id ?>/UnregisterPlayer/<?php echo $player->id ?>"><i class="far fa-trash-alt"></i></a></td>
				</tr>
			<?php
			} 
			?>
			</tbody>
		</table>
	</article>

	<section class="RegisterJoueur">
		<h2>Ajout d'un joueur</h2>
		<form class="form_register_player" method="post" action="/updateMatch/<?php echo $match->id ?>/RegisterPlayer">

			<label for="joueur">Enregistrer un joueur</label>
			<select id="joueur" name="joueur">
				<?php foreach($Allplayers as $player){	?>
					<option value="<?php echo $player->id ?>"><?php echo $player->nickname ?></option>
				<?php } ?>
			</select>

			<input type="submit" value="Enregister un joueur">

		</form>
			<div class="infos">

			<?php if ($nbPlayers < $game->min_players ) { ?>
				<p>Le nombre de joueurs est insuffisant pour débuter la partie. Veuillez patientez ...</p>
			<?php } else if ($nbPlayers >= $game->max_players) {?> 
				<p>le nombre de joueurs est atteint pour cette partie.</p>
			<?php
			}
			 ?>
		
				<?php if ($match->getStart_date() < new \DateTime()) {
				?>
				<form class="declare_winner" method="post" action="/updateMatch/<?php echo $match->id ?>/RegisterWinner">

					<label for="winner">Sélectionner le gagnant</label>
					<select id="winner" name="winner">
						<?php foreach($players as $player){	?>
							<option value="<?php echo $player->id ?>"><?php echo $player->nickname ?></option>
						<?php } ?>
					</select>
					
					<input type="submit" value="Déclarer">
				</form>

				<?php
			} 
			?>
			</div>
	</section>
</section>
