<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();

$routes->add('scoreboard', new Route('/', [
	'_controller' => 'Controller\PageController::scoreboard'
]));

$routes->add('AddGame', new Route('/ajouterJeux', [
	'_controller' => 'Controller\PageController::AjouterJeux'
]));

$routes->add('AddPlayers', new Route('/ajouterJoueurs', [
	'_controller' => 'Controller\PageController::AjouterJoueurs'
]));

$routes->add('AddContest', new Route('/ajouterMatch', [
	'_controller' => 'Controller\PageController::AjouterMatch'
]));

$routes->add('UpdateContest', new Route('/updateMatch/{id<\d+>}' , [
	'_controller' => 'Controller\PageController::UpdateMatch'
]));

$routes->add('RegisterPlayer', new Route('/updateMatch/{id<\d+>}/RegisterPlayer' , [
	'_controller' => 'Controller\PageController::RegisterPlayer'
]));

$routes->add('RegisterWinner', new Route('/updateMatch/{id<\d+>}/RegisterWinner' , [
	'_controller' => 'Controller\PageController::RegisterWinner'
]));

$routes->add('UnregisterPlayer', new Route('/updateMatch/{id_match<\d+>}/UnregisterPlayer/{id_player<\d+>}' , [
	'_controller' => 'Controller\PageController::UnregisterPlayer'
]));
return $routes;
