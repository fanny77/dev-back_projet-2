<?php

namespace Core;

use Symfony\Component\HttpFoundation\Response;

class AbstractController
{

	public function render(String $vue, Array $data = [])
	{
		extract($data);
		ob_start();
		include __DIR__.'/../Vues/'.$vue.'.php';
		return new Response(ob_get_clean());
	}

}
