<?php

namespace Model;

class Matchs
{
	public $id;
	public $start_date;

	public $game_id;
	public $winner_id;

	public function getGame()
	{
		return Jeux::getJeux($this->game_id);
	}

	public function getStart_date(){
		return \DateTime::createFromFormat("Y-m-d", $this->start_date);
	}

	public function getWinner() : ?Joueurs
	{
		if($this->winner_id != null)
			return Joueurs::getPlayer($this->winner_id);
		return null;
	}

	public function getNbPlayers() : int
	{
		$db = new Database();
		$connection = $db->getConnection();

		$result = $connection->query("SELECT COUNT(DISTINCT player_id) FROM player_contest WHERE contest_id = ".$this->id);
		return $result->fetchColumn();
	}

	public function getPlayers() : array
	{
		$db = new Database();
		$connection = $db->getConnection();

		$result = $connection->query("SELECT player.id, email, nickname FROM player, player_contest WHERE player.id = player_contest.player_id AND contest_id = ".$this->id);
		return $result->fetchAll(\PDO::FETCH_CLASS, self::class);

	}
	
	public function setGame_id($game_id) {
		$this->game_id = $game_id;
	}

	public function setStart_date($start_date) {
		$this->start_date = $start_date;
	}

	public function setWinner_id($winner_id) {
		$this->winner_id = $winner_id;
	}

	public static function getMatch($matchId) : Matchs
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query('SELECT id, game_id, start_date, winner_id FROM contest WHERE id = '.$matchId);
		return $result->fetchObject(self::class);
	}

	public static function getAllMatch()
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query(
			"SELECT contest.id AS id, start_date, winner_id, game_id
			FROM contest
			LEFT JOIN player_contest ON contest.id = player_contest.contest_id
			LEFT JOIN player ON player_contest.player_id = player.id
			LEFT JOIN game ON game.id = contest.game_id
			GROUP BY contest.id
			ORDER BY start_date DESC;"
		);
		return $result->fetchAll(\PDO::FETCH_CLASS, self::class);
	}

	public function save()
	{
		$db = new Database();
		$connection = $db->getConnection();

		if($this->id == NULL){
			$result = $connection->prepare('INSERT INTO contest (game_id, start_date, winner_id) VALUES (?,?,?)');
			$result->execute([
				$this->game_id,
				$this->start_date,
				$this->winner_id
			]);
		}
		else{
			$result = $connection->prepare('UPDATE contest SET game_id = ?, start_date = ?, winner_id = ? WHERE id = ?');
			$result->execute([
				$this->game_id,
				$this->start_date,
				$this->winner_id,
				$this->id
			]);
		}
		
		return $result;
	}

	public function RegisterPlayer($player_id) : bool {
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->prepare('INSERT INTO player_contest (player_id, contest_id) VALUES (?,?)');
		return $result->execute([
				$player_id,
				$this->id
			]);
	}

	public function RegisterWinner($player_id){
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->prepare('UPDATE contest SET winner_id = ? WHERE id = ?');
		return $result->execute([
				$player_id,
				$this->id
			]);
	}

	public function UnregisterPlayer($player_id){
		$db = new Database();
		$connection = $db->getConnection();
		return $result = $connection->query('DELETE FROM player_contest WHERE player_id = '.$player_id);
	}
}
