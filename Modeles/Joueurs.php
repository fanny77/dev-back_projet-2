<?php

namespace Model;


class Joueurs
{
	public $id;
	public $email;
	public $nickname;

	function getEmail(){
		return $this->email;
	}
	function getNickname(){
		return $this->nickname;
	}

	function setEmail($email) {
		$this->email = $email;
	}

	function SetNickname($nickname) {
		$this->nickname = $nickname;
	}

	public static function getPlayer($playerId)
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query('SELECT id, email, nickname FROM player WHERE id ='.$playerId);
		return $result->fetchObject(self::class);
	}
	public static function getAllPlayers()
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query('SELECT id, email, nickname FROM player ORDER BY id');
		return $result->fetchAll(\PDO::FETCH_CLASS, self::class);
	}

	public function save()
	{
		$db = new Database();
		$connection = $db->getConnection();

		if($this->id == NULL){
			$result = $connection->prepare('INSERT INTO player (email,nickname) VALUES (?,?)');
			$result->execute([
				$this->email,
				$this->nickname
			]);
		}
		else{
			$result = $connection->prepare('UPDATE player SET email = ?, nickname = ?) WHERE id = ?');
			$result->execute([
				$this->email,
				$this->nickname,
				$this->id
			]);
		}

		return $result;
	}
}
