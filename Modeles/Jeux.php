<?php

namespace Model;

class Jeux
{
	public $id;
	public $title;
	public $min_players;
	public $max_players;

	function getTitle(){
		return $this->title;
	}
	function getMin_playeurs(){
		return $this->min_players;
	}
	function getMax_playeurs(){
		return $this->max_players;
	}

	function setTitle($title) {
		$this->title = $title;
	}

	function setMin_players($min_players) {
		$this->min_players = $min_players;
	}

	function setMax_players($max_players) {
		$this->max_players = $max_players;
	}

	public static function getJeux($jeuxId) : ?Jeux
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query('SELECT id, title, min_players, max_players FROM game WHERE id = '.$jeuxId);

		return $result->fetchObject(self::class);
	}
	public static function getAllJeux()
	{
		$db = new Database();
		$connection = $db->getConnection();
		$result = $connection->query('SELECT id, title, min_players, max_players FROM game');

		return $result->fetchAll(\PDO::FETCH_CLASS, self::class);
	}

	public function save()
	{
		$db = new Database();
		$connection = $db->getConnection();

		if($this->id == NULL){
			$result = $connection->prepare('INSERT INTO game (title, min_players, max_players) VALUES (?,?,?)');
			$result->execute([
				$this->title,
				$this->min_players,
				$this->max_players
			]);
		}
		else{
			$result = $connection->prepare('UPDATE game SET title = ?, min_players = ?, max_players = ? WHERE id = ?');
			$result->execute([
				$this->title,
				$this->min_players,
				$this->max_players,
				$this->id
			]);
		}

		return $result;
	}
}

