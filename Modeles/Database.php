<?php

namespace Model;

use PDO;
use Exception;

class Database
{
	const DB_HOST = 'mysql:host=localhost;dbname=db_projet2;charset=utf8';
	const DB_USER = 'db_projet2';
	const DB_PASS = 'devback';
	public function getConnection(){
		//Tentative de connexion BDD
		try {
			$bdd = new PDO(self::DB_HOST,self::DB_USER,self::DB_PASS);
			$bdd->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			return $bdd;
		} catch (Exception $errorConnection) {
			die ('Erreur de connection :'.$errorConnection->getMessage());
		}
	}
}

?>