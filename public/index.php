<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

$request = Request::createFromGlobals();

$routes = require __DIR__.'/../routes/routes.php';

try {

	$context = new RequestContext();
	$context->fromRequest($request);

	$match = (new UrlMatcher($routes, $context))->matchRequest($request);
	$request->attributes->add($match);

	// Syntaxe Déballage des tableaux
	[$className, $methodName] = explode("::", $match['_controller']);
	$controller = [new $className, $methodName];

	$response = call_user_func($controller, $request);

} catch(ResourceNotFoundException $error404) {
	$response = new Response("Page inexistante", 404);
}

$response->send();
