<?php

namespace Controller;

use Model\Jeux;
use Model\Matchs;
use Model\Joueurs;
use Core\AbstractController;
use PDOException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class PageController extends AbstractController {

	public function scoreboard() {
		$games = Jeux::getAllJeux();
		$players = Joueurs::getAllPlayers();
		$matchs = Matchs::getAllMatch();

		return $this->render('scoreboard', [
			'games' => $games,
			'players' => $players,
			'matchs' => $matchs
		]);
	}

	public function AjouterJeux(Request $request) {

		if($request->isMethod("POST"))
		{
			$name = $request->request->filter("name");
			$min_p = $request->request->filter("min_players");
			$max_p = $request->request->filter("max_players");

			if(
				!empty($name)
				&& !empty($min_p)
				&& !empty($max_p)
				&& is_numeric($min_p)
				&& is_numeric($max_p)
			){
				// on insert dans la base

				$jeu = new Jeux();
				$jeu->setTitle($name);
				$jeu->setMax_players($max_p);
				$jeu->setMin_players($min_p);

				if($jeu->save()){
					return new RedirectResponse("/");
				}
			}
		}

		return $this->render('Ajouter_jeux');
	}
	public function AjouterJoueurs(Request $request) {
		
		if($request->isMethod("POST"))
		{
			$email = $request->request->filter("email");
			$nickname = $request->request->filter("nickname");
			
			if(
				!empty($email)
				&& !empty($nickname)
				&& is_string($email)
				&& is_string($nickname)
			){
				// on insert dans la base
				
				$joueur = new Joueurs();
				$joueur->setEmail($email);
				$joueur->SetNickname($nickname);
				
				if($joueur->save()){
					return new RedirectResponse("/");
				}
			}
		}
		
		return $this->render('Ajouter_joueurs');

	}
	public function AjouterMatch(Request $request) {

		$games = Jeux::getAllJeux();
		
		if($request->isMethod("POST"))
		{
			$jeux = (int) $request->request->filter("jeux");
			$start_date = $request->request->filter("date");
			
			if(
				!empty($jeux)
				&& !empty($start_date)
				&& is_string($start_date)
			){
				// on insert dans la base
				
				$match = new Matchs();
				$match->setGame_id($jeux);
				$match->setStart_date($start_date);
				
				if($match->save()){
					return new RedirectResponse("/");
				}
			}
		}

		return $this->render('Ajouter_matchs', ["games" => $games]);
	}

	public function UpdateMatch(Request $request) {
		$id = (int) $request->attributes->filter('id');

		$match = Matchs::getMatch($id);
		$nbPlayers = $match->getNbPlayers();
		$Allplayers = Joueurs::getAllPlayers();

		return $this->render('Update_match', [
			'players' => $match->getPlayers(),
			'match' => $match,
			'game' => $match->getGame(),
			'Allplayers' => $Allplayers,
			'nbPlayers' => $nbPlayers
		]);

	}

	public function RegisterPlayer(Request $request) {
		
		$match_id = (int) $request->attributes->filter('id');
		$match = Matchs::getMatch($match_id);

		if($request->isMethod("POST"))
		{	
			$player_id = $request->request->filter("joueur");
			
			if(
				!empty($player_id)
				&& is_numeric($player_id)
				&& $match->getGame()->getMax_playeurs() > $match->getNbPlayers()
			){
				// on insert dans la base
				try{
					$match->RegisterPlayer($player_id);
				} catch(PDOException $e){}
			}
		}

		return new RedirectResponse("/updateMatch/".$match_id);
	}
	public function RegisterWinner(Request $request) {
			
		$match_id = (int) $request->attributes->filter('id');

		if($request->isMethod("POST"))
		{
			$player_id = $request->request->filter("winner");

			if(
				!empty($player_id)
				&& is_numeric($player_id)
			){
				// on insert dans la base
				
				$match = new Matchs();
				$match->id = (int) $match_id;
				
				$match->RegisterWinner($player_id);
			}
		}
		return new RedirectResponse("/updateMatch/".$match_id);
	}

	public function UnregisterPlayer(Request $request){
		
		$match_id = (int) $request->attributes->filter('id_match');
		$player_id = (int) $request->attributes->filter('id_player');

		$match = Matchs::getMatch($match_id);
		$match->UnregisterPlayer($player_id);

		return new RedirectResponse("/updateMatch/".$match_id);
	}
}
